FROM markhobson/maven-chrome:jdk-8
WORKDIR /usr/app
COPY ./pom.xml ./pom.xml
RUN mvn clean install -DskipTests=true
COPY . .
CMD mvn test