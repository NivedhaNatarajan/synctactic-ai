package dash.syntactic.ai.framework.init;

import dash.syntactic.ai.framework.common.Generics;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeSuite;

public class APIBaseClass {
    public Response response = null;

    @BeforeSuite(alwaysRun = true)
    public void getInit(){
        RestAssured.baseURI = Generics.URL;
    }
}
