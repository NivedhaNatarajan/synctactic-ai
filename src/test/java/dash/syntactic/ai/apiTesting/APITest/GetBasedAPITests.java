package dash.syntactic.ai.apiTesting.APITest;

import dash.syntactic.ai.framework.init.APIBaseClass;
import dash.syntactic.ai.uiTesting.constants.Constants;
import io.restassured.RestAssured;
import org.testng.annotations.Test;

public class GetBasedAPITests extends APIBaseClass implements Constants {

    @Test
    public void validateGet() {

        response = RestAssured.given().auth().preemptive().basic(LOGIN_USERNAME,LOGIN_PASSWORD)
                    .when()
                    .get("/auth/sign-in");
        System.out.println("Response Status Code : " + response.getStatusCode());
        System.out.println("Response Body : " + response.body().asPrettyString());
    }
}
