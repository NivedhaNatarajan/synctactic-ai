package dash.syntactic.ai.uiTesting.pageObjects.common.Verification;

import dash.syntactic.ai.framework.init.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class LoginVerification extends AbstractPage {

    public LoginVerification (WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "(//div[@id='root']//img)[1]")
    private WebElement txtLogo;

    @FindBy(xpath = "//label[text()='Username']//parent::div/div/input")
    private WebElement txtbxUserName;

    @FindBy(xpath = "//label[text()='Password']//parent::div/div/input")
    private WebElement txtbxPassword;

    @FindBy(xpath = "//a[text()='Discover']")
    private WebElement txtDiscover;

    @FindBy(xpath = "//button[text()='Login']")
    private WebElement btnLogin;

    public WebElement getLogo () {
        return txtLogo;
    }
    public WebElement getLogin () { return btnLogin; }
    public WebElement getUserName () {
        return txtbxUserName;
    }
    public WebElement getPassword () {
        return txtbxPassword;
    }
    public WebElement getDiscover () {
        return txtDiscover;
    }

    public boolean verifyLoginSuccessful() {
        return isElementPresent(getDiscover());
    }

    public void verifyLoginPage() {
        if (isElementPresent(getLogo())) {
            System.out.println();
            testInfoLog("Application", "Syntactic.ai is loaded");
        }
    }

    public void verifyLoginDetails(String username, String password) {
        Assert.assertEquals(getValue(getUserName()), username, "Username is not Matching");
        Assert.assertEquals(getValue(getPassword()), password, "Password is not Matching");
        testVerifyLog("Login details verified");
    }
}

