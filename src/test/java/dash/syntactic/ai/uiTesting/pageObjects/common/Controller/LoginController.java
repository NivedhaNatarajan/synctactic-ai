package dash.syntactic.ai.uiTesting.pageObjects.common.Controller;

import dash.syntactic.ai.framework.init.AbstractPage;
import dash.syntactic.ai.uiTesting.pageObjects.common.Verification.LoginVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class LoginController extends AbstractPage {

    LoginVerification loginVerification = new LoginVerification(driver);

    public LoginController (WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "(//div[@id='root']//img)[1]")
    private WebElement txtLogo;

    @FindBy(xpath = "//label[text()='Username']//parent::div/div/input")
    private WebElement txtbxUserName;

    @FindBy(xpath = "//label[text()='Password']//parent::div/div/input")
    private WebElement txtbxPassword;

    @FindBy(xpath = "//button[text()='Login']")
    private WebElement btnLogin;

    @FindBy(xpath = "//div[@id='root']//div/img[@class='ui avatar image']")
    private WebElement imgUserSetting;

    @FindBy(xpath = "//div[@id='root']//span[text()='Logout']")
    private WebElement btnLogout;

    public WebElement getLogo () {
        return txtLogo;
    }

    public WebElement getUserName () {
        return txtbxUserName;
    }

    public WebElement getPassword () {
        return txtbxPassword;
    }

    public WebElement getLogin () {
        return btnLogin;
    }

    public WebElement getLogout () { return btnLogout; }

    public WebElement getUserSettings () { return imgUserSetting; }

    public String loginAs(String username, String password) {
        loginVerification.verifyLoginPage();
        enterUsername(username);
        enterPassword(password);
        loginVerification.verifyLoginDetails(username, password);
        clickOnLoginButton();
        return String.valueOf(loginVerification.verifyLoginSuccessful());
    }

    public void logout() {
        getExplicitWait(driver, getUserSettings(), EXPLICIT_WAIT);
        clickOn(driver, getUserSettings());
        clickOn(driver, getLogout());
        Assert.assertTrue(isElementPresent(getLogo()), "Logout Failed");
        testStepsLog(_logStep++, "Logged out of the Application");
    }

    public void enterUsername(String username) {
        testStepsLog(_logStep++, "Enter Username : " + username);
        type(getUserName(), username);
    }

    public void enterPassword(String password) {
        testStepsLog(_logStep++, "Enter Password : " + password);
        type(getPassword(), password);
    }

    public boolean clickOnLoginButton() {
        testStepsLog(_logStep++, "Click on Login button");
        return getClickOnTryCatch(driver, getLogin());
    }

    public boolean loginAsValidation(String username, String password) {
        loginVerification.verifyLoginPage();
        enterUsername(username);
        enterPassword(password);
        loginVerification.verifyLoginDetails(username, password);
        if(clickOnLoginButton()) {
            testValidationLog("Valid Credentials - Login button is Enabled");
            return true;
        } else {
            testValidationLog("Invalid Credentials - Login button is Disabled");
            return false;
        }
    }
}
