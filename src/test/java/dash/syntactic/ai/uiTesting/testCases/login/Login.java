package dash.syntactic.ai.uiTesting.testCases.login;

import dash.syntactic.ai.framework.init.WebDriverInit;
import dash.syntactic.ai.uiTesting.constants.Constants;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;

public class Login extends WebDriverInit implements Constants {

    HashMap<String, ArrayList<Object>> map = new HashMap<>();
    String check;
    String username;
    String password;
    String status;

    @Test
    public void getLogin()
    {
        testCaseLog("Login");
        map.putAll(getTestDataWorkbook(DATA_LOGIN));
        for (int i=0; i<map.get(LOGIN_USERNAME).size(); i++) {
            username = getData(map, LOGIN_USERNAME, i);
            password = getData(map, LOGIN_PASSWORD, i);
            status = getData(map, LOGIN_STATUS, i);
            loginController.loginAsValidation(username, password);
            check = getString(loginVerification.verifyLoginSuccessful());
            if (check.equalsIgnoreCase(status)) {
                if (status.equalsIgnoreCase("TRUE")) {
                    testVerifyLog("Logged into the application");
                    loginController.logout();
                } else {
                    testVerifyLog("Wrong credentials - Login Failed");
                }
                stepPassed();
            } else {
                testWarningLog("Test Failed - Expected and Actual Output is not Matching");
                testInfoLog("Expected Result", status.toUpperCase());
                testInfoLog("Actual Result", check.toUpperCase());
                stepFailure(driver);
            }
            openURL(driver, URL);
        }
    }
}
