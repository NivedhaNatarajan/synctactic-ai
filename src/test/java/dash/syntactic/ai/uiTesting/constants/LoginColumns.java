package dash.syntactic.ai.uiTesting.constants;

public interface LoginColumns {
    String LOGIN_USERNAME = "Username";
    String LOGIN_PASSWORD = "Password";
    String LOGIN_STATUS = "Status";
}
